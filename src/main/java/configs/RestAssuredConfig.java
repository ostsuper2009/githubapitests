package configs;

import io.restassured.RestAssured;

public class RestAssuredConfig {

    public static void setUp() {
        RestAssured.baseURI = "https://api.github.com";
        RestAssured.basePath = "/";
    }
}
