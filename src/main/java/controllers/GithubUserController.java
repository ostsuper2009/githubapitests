package controllers;

import models.GithubUser;
import org.springframework.web.client.RestTemplate;

public class GithubUserController {
    private RestTemplate restTemplate;
    public static final String BASE_API_URL = "https://api.github.com";

    public GithubUserController() {
        restTemplate = new RestTemplate();
    }

    public GithubUser getUserbyUsername(String username) {
        try {
            return restTemplate.getForObject(BASE_API_URL + "/users/" + username, GithubUser.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public GithubUser[] getUserFollowers(String username) {
        try {
            return restTemplate.getForObject(BASE_API_URL + "/users/" + username + "/followers",  GithubUser[].class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
