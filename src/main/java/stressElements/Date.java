package stressElements;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.text.SimpleDateFormat;

public class Date extends ServerResource {

    @Get
    public String getDate() {
       String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());

       return date;
    }
}
