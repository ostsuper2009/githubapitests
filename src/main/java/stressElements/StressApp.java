package stressElements;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

public class StressApp extends Application {
    static Component component;
    public static void runserver(int port) {
         component = new Component();
        Application application = new StressApp();
        component.getLogService();
        component.getServers().add(Protocol.HTTP, port);
        String rootContext = "/service";
        component.getDefaultHost().attach(rootContext, application);
        try {
            component.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public Restlet createInboundRoot() {
        Router router = new Router(getContext());
        router.attach("/test", new StressRestlet());
        router.attach("/date", Date.class);

        return router;
    }

    public static void stopServer() {
        try {
            component.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
