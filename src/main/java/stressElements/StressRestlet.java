package stressElements;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.MediaType;

public class StressRestlet extends Restlet {

    @Override
    public void handle(Request request, Response response) {
        response.setEntity("LOAD-TEST", MediaType.TEXT_PLAIN);
    }
}
