package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import ui.setup.CustomLoadebleComponent;

import static ui.setup.SeleniumDriver.getDriver;

public abstract class BaseObjectPage <T extends CustomLoadebleComponent<T>> extends CustomLoadebleComponent<T>{
    private WebDriver driver;
    private static final String BASE_URL = "https://github.com";

    public BaseObjectPage(WebDriver driver) {
        this.driver = driver;
    }

    public T openPage(Class<T> tClass) {
        T page = PageFactory.initElements(getDriver(), tClass);
        getDriver().get(BASE_URL + getPageUrl());
        return page.get();
    }

    protected abstract String getPageUrl();

    public void open(String url) {
        driver.get(url);
    }
    public WebElement find(By locator) {
        return driver.findElement(locator);
    }

    public void type(By inputLocator, String text) {
        find(inputLocator).sendKeys(text);
    }

    public void type(WebElement input, String text) {
        input.sendKeys(text);
    }

    public void click(By locator) {
        find(locator).click();
    }

    public void click(WebElement element) {
        element.click();
    }

    public boolean isElementDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}