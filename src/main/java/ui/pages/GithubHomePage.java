package ui.pages;

import org.openqa.selenium.By;
import ui.setup.PageLoadHelper;

import static ui.setup.SeleniumDriver.getDriver;

public class GithubHomePage extends BaseObjectPage<GithubHomePage> {

    public GithubHomePage() {
        super(getDriver());
    }

    public GithubLoginPage goToLoginPage() {
        return new GithubLoginPage().openPage(GithubLoginPage.class);
    }

    public GithubHomePage open() {
        return new GithubHomePage().openPage(GithubHomePage.class);
    }
    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() throws Error {
        PageLoadHelper.isLoaded().
                isElementIsVisible(By.cssSelector("input[name='user[login]']")).
                isElementIsClicable(By.cssSelector("input[name='user[login]']"));
    }

    @Override
    protected String getPageUrl() {
        return "";
    }
}
