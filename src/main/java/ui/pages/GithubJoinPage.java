package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.setup.PageLoadHelper;

import static ui.setup.SeleniumDriver.getDriver;

public class GithubJoinPage extends BaseObjectPage<GithubJoinPage>{

    @FindBy(id = "user_login")
    private WebElement usernameField;

    @FindBy(id = "user_email")
    private WebElement emailField;

    @FindBy(id = "user_password")
    private WebElement passwordField;

    @FindBy(className = "flash-error")
    private WebElement errorBox;

    @FindBy(id = "signup_button")
    private WebElement joinButton;

    public GithubJoinPage() {
        super(getDriver());
    }

    public void registerNewUser(String username, String email, String password) {
        type(usernameField, username);
        type(emailField, email);
        type(passwordField, password);
        click(joinButton);
    }

    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() throws Error {
        PageLoadHelper.isLoaded().
                isElementIsVisible(By.cssSelector("input[id='user_login']")).
                isElementIsClicable(By.cssSelector("input[id='user_login']"));
    }

    @Override
    protected String getPageUrl() {
        return "/join";
    }

    public boolean isLoginError() {
        return isElementDisplayed(errorBox);
    }
}
