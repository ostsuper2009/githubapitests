package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.setup.PageLoadHelper;

import static ui.setup.SeleniumDriver.getDriver;

public class GithubLoginPage extends BaseObjectPage<GithubLoginPage> {

    @FindBy(id = "login_field")
    private WebElement loginField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(name = "commit")
    private WebElement commitButton;

    @FindBy(className = "flash-error")
    private WebElement errorBox;

    @FindBy(xpath = "//*[@id=\"user-links\"]/li[3]/a/img")
    private WebElement avatarImage;

    public GithubLoginPage() {
        super(getDriver());
    }

    @Override
    protected void load() {
    }

    @Override
    protected void isLoaded() throws Error {
        PageLoadHelper.isLoaded().
                isElementIsVisible(By.cssSelector("#login_field")).
                isElementIsClicable(By.cssSelector("#login_field"));
    }

    @Override
    protected String getPageUrl() {
        return "/login";
    }

    public void login(String userName, String password) {
        type(loginField, userName);
        type(passwordField, password);
        click(commitButton);
    }

    public boolean isLoginError() {
        return isElementDisplayed(errorBox);
    }

    public String getErrorMessage() {
        return errorBox.getText();
    }

    public boolean isLoginSuccess() {
        return isElementDisplayed(avatarImage);
    }
}
