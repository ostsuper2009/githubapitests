package ui.setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

import static ui.setup.SeleniumDriver.getDriver;

public abstract class CustomLoadebleComponent<T extends CustomLoadebleComponent<T>> {
    private WebDriver driver;

    private static final int LOAD_TIMEOUT = 30;
    private static final int REFRESH_RATE = 2;

    public T get() {
        try {
            isLoaded();
            return (T) this;
        } catch (Error e) {
            System.out.println("Error encountered during page load: " + e.getMessage());
            load();
        }
        isLoaded();
        return (T) this;
    }

    protected abstract void load();
    protected abstract void isLoaded() throws Error;

    protected void waitForPageLoad(ExpectedCondition pageloadCondition) {
        Wait wait = new FluentWait(getDriver())
                .withTimeout(LOAD_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(REFRESH_RATE, TimeUnit.SECONDS);
        wait.until(pageloadCondition);
    }
}
