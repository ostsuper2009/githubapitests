import controllers.GithubUserController;
import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import models.GithubUser;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.core.Is.is;

public class GithubUserInfoTest {
    private GithubUserController controller;

    @BeforeTest
    public void setUp() {
        controller = new GithubUserController();
    }

    @Test
    public void testUserInfoResponse() {
        GithubUser githubUser = controller.getUserbyUsername("ostsuper2009");
        assertThat(githubUser.getHtml_url(), containsString("https://github.com/ostsuper2009"));
        assertThat(githubUser.getId(), is(6019940));
        assertThat(githubUser.getLogin(), is("ostsuper2009"));
    }

    @Test
    public void testUserFollowers()
    {
        GithubUser[] followers = controller.getUserFollowers("ostsuper2009");
        assertThat(followers, is(emptyArray()));
    }
}
