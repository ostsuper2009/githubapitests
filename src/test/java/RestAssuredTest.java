
import configs.RestAssuredConfig;
import controllers.GithubUserController;
import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import io.restassured.config.HttpClientConfig;
import io.restassured.response.Response;
import models.GithubUser;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;

public class RestAssuredTest {

    @BeforeTest
    public void setUp() {
        RestAssuredConfig.setUp();
        RestAssured.config = io.restassured.config.RestAssuredConfig.config().connectionConfig(new ConnectionConfig().closeIdleConnectionsAfterEachResponse());

    }

    @Test
    public void testGetUserInfo() {
        RestAssured.config = io.restassured.config.RestAssuredConfig.newConfig().httpClient(HttpClientConfig.httpClientConfig().reuseHttpClientInstance());
        GithubUser response =
                given().
                        baseUri(GithubUserController.BASE_API_URL).
                when().
                        get("users/ostsuper2009").
                then().assertThat().statusCode(200).
                        extract().response().as(GithubUser.class);
        String testAvatarUrl = response.getAvatarUrl();
        String expectedAvatarUrl = "https://avatars1.githubusercontent.com/u/6019940?v=4";
        assertThat(testAvatarUrl, is(expectedAvatarUrl));
    }

    @Test
    public void testGetFollowersByUser() {
        RestAssured.config = io.restassured.config.RestAssuredConfig.newConfig().httpClient(HttpClientConfig.httpClientConfig().reuseHttpClientInstance());
        Response response =
                given().
                        baseUri(GithubUserController.BASE_API_URL).
                when().
                        get("users/MaxPresman/followers").
                then().
                        assertThat().statusCode(200).extract().response();
        List<String> loginNamesOfFollowers = response.path("login");
      //  System.out.println(loginNamesOfFollowers);
        assertThat(loginNamesOfFollowers, not(empty()));
        assertThat(loginNamesOfFollowers, hasSize(30));
    }

    @Test
    public void testBasicAuth()
    {
        RestAssured.config = io.restassured.config.RestAssuredConfig.newConfig().httpClient(HttpClientConfig.httpClientConfig().reuseHttpClientInstance());
        Response response =
                given().
                        baseUri(GithubUserController.BASE_API_URL).
                        when().
                        queryParam("access_token","f642127c6ce22b0958417c0008cb005666435e74").
                        get(GithubUserController.BASE_API_URL).
                        then().
                        extract().response();
        System.out.println(response.asString());
    }

    @AfterTest
    public void tearDown() {
        RestAssured.reset();
    }
}
