import io.restassured.config.HttpClientConfig;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matchers;
import org.restlet.Response;
import stressElements.StressApp;
import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import io.restassured.config.RestAssuredConfig;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.testng.Assert.assertEquals;


public class StressRestAssuredTest {
    static final int wait = 60*1000;
    private int iterations = 10;
    private String post = "test";
    private String expect = "LOAD-TEST";
    private String url = null;
    private static final int PORT = 8082;
    private static final String BASE_URL = "http://localhost:8082/service";

    @BeforeTest
    public void setUp() {
        url = "http://localhost:8082/service/test";
        StressApp.runserver(PORT);
       // RestAssured.config = RestAssuredConfig.config().connectionConfig(new ConnectionConfig().closeIdleConnectionsAfterEachResponse());
    }

    @AfterTest
    public void tearDown() throws Exception {
        StressApp.stopServer();
        RestAssured.reset();
    }

    @Test
    public void testStressGetRequest() {
        for (int i = 0, n = iterations; i < n; i++) {
            given().
                    expect().body(equalTo(expect)).
            when().
                    get(url).
            then().
                    assertThat().statusCode(equalTo(200));
        }
    }

    @Test
    public void testStressPostRequest() throws UnsupportedEncodingException {
        for (int i = 0, n = iterations; i < n; i++) {
            given().
                    contentType("text/plain;charset=utf-8").body(post.getBytes("UTF-8")).
                    expect().body(equalTo(expect)).
            when().
                    post(url).
            then().
                    assertThat().statusCode(200);
        }
    }

    @Test
    public void stressWithRestAssuredPostWhenSameHttpClientInstanceIsReused() throws UnsupportedEncodingException {
        RestAssured.config = RestAssuredConfig.newConfig().httpClient(HttpClientConfig.httpClientConfig().reuseHttpClientInstance());

        try {
            for (int i = 0, n = iterations; i < n; i++) {
                given().contentType("text/plain;charset=utf-8").body(post.getBytes("UTF-8")).
                        expect().body(equalTo(expect)).
                        when().post(url);
            }
        } finally {
            RestAssured.reset();
        }
    }

    @Test
    public void stressWithRestAssuredGetManualClose() throws IOException, InterruptedException {
        RestAssured.config = RestAssuredConfig.newConfig().httpClient(HttpClientConfig.httpClientConfig().reuseHttpClientInstance());

        try {
            for (int i = 0, n = iterations; i < n; i++) {
                String body = IOUtils.toString(get(url).andReturn().body().asInputStream());
                assertEquals(expect, body);
            }
        } finally {
            RestAssured.reset();
        }
    }

    @Test
    public void testDate() {
        for (int i = 0, n = iterations; i < n; i++) {
            String str =
            given().
                    baseUri(BASE_URL).
            when().
                    get("/date").
            then().
                    assertThat().statusCode(200).extract().asString();
            assertThat(str, containsString("2017"));

        }
    }




}
