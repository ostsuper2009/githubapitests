package ui.tests;

import org.testng.annotations.Test;
import ui.pages.GithubHomePage;
import ui.pages.GithubLoginPage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class GitHubLoginTest extends SeleniumBaseTest {

    @Test
    public void shouldBeErrorWithInvalidLoginCredentials() {
        GithubHomePage githubHomePage = new GithubHomePage().open();
        GithubLoginPage loginPage = githubHomePage.goToLoginPage();
        loginPage.login("user1", "password1");
        assertTrue("Error message was not displayed!", loginPage.isLoginError());
        assertEquals(loginPage.getErrorMessage(), "Incorrect username or password.", "Error message was incorrect!");
    }

    @Test
    public void shouldBeSuccessLogin() {
        GithubHomePage githubHomePage = new GithubHomePage().open();
        GithubLoginPage loginPage = githubHomePage.goToLoginPage();
        loginPage.login("ostsuper2009", "123456qaz");
        assertTrue(loginPage.isLoginSuccess());
        assertFalse(loginPage.isLoginError());
    }
}
