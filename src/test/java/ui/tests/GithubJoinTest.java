package ui.tests;

import org.testng.annotations.Test;
import ui.pages.GithubJoinPage;

import static org.testng.AssertJUnit.assertTrue;

public class GithubJoinTest extends SeleniumBaseTest {

    @Test
    public void testThatUsernameIsExist() {
        GithubJoinPage joinPage = new GithubJoinPage().openPage(GithubJoinPage.class);
        joinPage.registerNewUser("user", "emial@mail.com", "password");
        assertTrue("Error message should be visible!", joinPage.isLoginError());
    }
}
