package ui.tests;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import static ui.setup.SeleniumDriver.getDriver;

public class SeleniumBaseTest {

    @BeforeTest
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
    }

    @AfterTest
    public static void tearDownClass() {
        getDriver().close();
    }
    @Before
    public void setUp() {
        getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        if (getDriver() != null) {
            getDriver().quit();
        }
    }
}
